from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from configurations.views import AppsConfigView, BitbucketDetailView, BugzillaDetailView, GerritDetailView, \
    GitDetailView, JenkinsDetailView
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Configurations API')

urlpatterns = [
    path('', schema_view),
    path('configurations', AppsConfigView.as_view()),
    path('configurations/bitbucket', BitbucketDetailView.as_view()),
    path('configurations/bugzilla', BugzillaDetailView.as_view()),
    path('configurations/gerrit', GerritDetailView.as_view()),
    path('configurations/git', GitDetailView.as_view()),
    path('configurations/jenkins', JenkinsDetailView.as_view()),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
