from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from configurations.serializers import AppsConfigSerializer, AppsDetailSerializer
from configurations.models import AppsConfig, Bitbucket, Bugzilla, Gerrit, Git, Jenkins


class AppsConfigView(APIView):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.model = AppsConfig
        self.serializer = AppsConfigSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer(*args, **kwargs)

    def get(self, request):
        user = request.GET.get('user', None)
        config = self.model.objects.all()
        config_data = []
        if not user:
            serializer = self.serializer(config, many=True)
            config_data.extend(serializer.data)
        else:
            for c in config:
                try:
                    data = globals()[c.name].objects.get(user=user)
                    AppsDetailSerializer.Meta.model = globals()[c.name]
                    serializer = AppsDetailSerializer(data)
                    config_data.append(serializer.data)
                except ObjectDoesNotExist:
                    pass
        return Response(config_data, status=status.HTTP_200_OK)

    def put(self, request):
        name = request.data.get('name', None)
        config = get_object_or_404(self.model, name=name)
        serializer = self.serializer(config, data=request.data, partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request):
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_403_FORBIDDEN)

    def delete(self, request):
        name = request.data.get('name', None)
        config = get_object_or_404(self.model, name=name)
        config.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class AppsDetailView(APIView):

    def __init__(self, model, app_name, **kwargs):
        super().__init__(**kwargs)
        self.model = model
        self.app = get_object_or_404(AppsConfig, name=app_name)
        AppsDetailSerializer.Meta.model = self.model
        self.serializer = AppsDetailSerializer

    def get_serializer(self, *args, **kwargs):
        return self.serializer(*args, **kwargs)

    def post(self, request):
        serializer = self.serializer(data=request.data)
        if serializer.is_valid():
            serializer.save(app_name=self.app)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_403_FORBIDDEN)

    def put(self, request):
        user = request.data.get('user', None)
        user_data = get_object_or_404(self.model, user=user)
        serializer = self.serializer(user_data, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def delete(self, request):
        user = request.data.get('user', None)
        user_data = get_object_or_404(self.model, user=user)
        user_data.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


class BitbucketDetailView(AppsDetailView):

    def __init__(self, **kwargs):
        self.model = Bitbucket
        self.app_name = 'Bitbucket'
        super().__init__(self.model, self.app_name, **kwargs)


class BugzillaDetailView(AppsDetailView):

    def __init__(self, **kwargs):
        self.model = Bugzilla
        self.app_name = 'Bugzilla'
        super().__init__(self.model, self.app_name, **kwargs)


class GerritDetailView(AppsDetailView):

    def __init__(self, **kwargs):
        self.model = Gerrit
        self.app_name = 'Gerrit'
        super().__init__(self.model, self.app_name, **kwargs)


class GitDetailView(AppsDetailView):

    def __init__(self, **kwargs):
        self.model = Git
        self.app_name = 'Git'
        super().__init__(self.model, self.app_name, **kwargs)


class JenkinsDetailView(AppsDetailView):

    def __init__(self, **kwargs):
        self.model = Jenkins
        self.app_name = 'Jenkins'
        super().__init__(self.model, self.app_name, **kwargs)
