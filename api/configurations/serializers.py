from rest_framework import serializers
from configurations.models import AppsConfig

apps = [
    ('Bitbucket', 'Bitbucket'),
    ('Bugzilla', 'Bugzilla'),
    ('Gerrit', 'Gerrit'),
    ('Git', 'Git'),
    ('Jenkins', 'Jenkins'),
]


class AppsConfigSerializer(serializers.ModelSerializer):
    name = serializers.ChoiceField(choices=apps)
    created_on = serializers.DateTimeField(read_only=True)
    modified_on = serializers.DateTimeField(read_only=True)

    class Meta:
        model = AppsConfig
        fields = '__all__'


class AppsDetailSerializer(serializers.ModelSerializer):
    app_name = serializers.PrimaryKeyRelatedField(read_only=True)
    created_on = serializers.DateTimeField(read_only=True)
    modified_on = serializers.DateTimeField(read_only=True)

    class Meta:
        model = None
        fields = '__all__'
