from django.db import models


class AppsConfig(models.Model):
    name = models.CharField(max_length=50, primary_key=True)
    url = models.URLField(default='', blank=True)
    image = models.ImageField(upload_to='app-icons', default="app-icons/apps.png")
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)


class Bitbucket(models.Model):
    user = models.CharField(max_length=50, primary_key=True)
    app_name = models.ForeignKey(AppsConfig, on_delete=models.CASCADE)
    username = models.CharField(max_length=50, null=False)
    password = models.CharField(max_length=50, null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)


class Git(models.Model):
    user = models.CharField(max_length=50, primary_key=True)
    app_name = models.ForeignKey(AppsConfig, on_delete=models.CASCADE)
    username = models.CharField(max_length=50, null=False)
    password = models.CharField(max_length=50, null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)


class Gerrit(models.Model):
    user = models.CharField(max_length=50, primary_key=True)
    app_name = models.ForeignKey(AppsConfig, on_delete=models.CASCADE)
    username = models.CharField(max_length=50, null=False)
    password = models.CharField(max_length=50, null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)


class Bugzilla(models.Model):
    user = models.CharField(max_length=50, primary_key=True)
    app_name = models.ForeignKey(AppsConfig, on_delete=models.CASCADE)
    username = models.CharField(max_length=50, null=False)
    password = models.CharField(max_length=50, null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)


class Jenkins(models.Model):
    user = models.CharField(max_length=50, primary_key=True)
    app_name = models.ForeignKey(AppsConfig, on_delete=models.CASCADE)
    username = models.CharField(max_length=50, null=False)
    password = models.CharField(max_length=50, null=False)
    created_on = models.DateTimeField(auto_now_add=True)
    modified_on = models.DateTimeField(auto_now=True)
